﻿using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public ProductController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("ProductList")]
        [Authorize]
        public IActionResult getProduct()
        {
            var result = _authentication.getProductList<ModelProduct>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelProduct product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", product.IDCompany);
            dp_param.Add("idbrand", product.IDBrand);
            dp_param.Add("name", product.Name);
            dp_param.Add("variant", product.Variant);
            dp_param.Add("price", product.Price);
            dp_param.Add("iduser", product.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelProduct>("sp_createProduct", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = product, message = "Success", code = 200 });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelProduct product, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("idcompany", product.IDCompany);
            dp_param.Add("idbrand", product.IDBrand);
            dp_param.Add("name", product.Name);
            dp_param.Add("variant", product.Variant);
            dp_param.Add("price", product.Price);
            dp_param.Add("iduser", product.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);


            var result = _authentication.Execute_Command<ModelProduct>("sp_updateProduct", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
